##Future Improvements

#General Features:

- Display data for more than last 20 games
- Graph/display more statistics
    - Timeline of a match in reference to others in a match
    - K/D/A and CS over time
    - Detailed match breakdowns
- Search - add text debounce (eliminating the need for search button)
- Loading screen / indication
- Account Sign-up (maybe using SSO) to allow for Users to customize data shown on their dashboard
- Gather User metrics via Mixpanel

#Architecture:

- Persist all raw data retrieved from the LoL API (using MongoDB)
- Cache Account info
    - Store last sync'd date/match date per account
- Store precomputed stat values into Redis, when retrieving account
- Dockerize the two projects
- Recursive data retrieval for accounts

#Code Improvements:

- Tests
    - Add integration tests / unit tests via mocha
    - UI testing via selenium
        - Testing via Browser testing
- Breaking up code into smaller modules and React components
    - App.js is too large
- Commenting
- Linting / code style standards
- Use Redux

const Boom = require('boom');

module.exports = lolClient => ({
    method: 'GET',
    path: '/v1/lol/matches/detailed/getByAccount/{accountId}',
    handler({ params: { accountId } }, reply) {
        return lolClient.Match.gettingRecentListByAccount(accountId)
            .then((matchList) => {
                const detailedMatchPromises = matchList.matches.map(match =>
                    lolClient.Match.gettingById(match.gameId)
                        .then((matchDetails) => {
                            match.matchDetails = matchDetails;
                            return match;
                        }));
                return Promise.all(detailedMatchPromises);
            })
            .then(matches => reply(matches))
            .catch(err => reply(Boom.badImplementation(err)));
    },
});

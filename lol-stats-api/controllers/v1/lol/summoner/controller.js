const Boom = require('boom');

module.exports = lolClient => ({
    method: 'GET',
    path: '/v1/lol/summoner/getByName/{summonerName}',
    handler({ params: { summonerName } }, reply) {
        return lolClient.Summoner.gettingByName(summonerName)
            .then((summoner) => {
                if (!summoner) {
                    return reply(Boom.notFound(`Summoner was not found ${summonerName} not found`));
                }
                return reply(summoner);
            })
            .catch(err => reply(Boom.badImplementation(err)));
    },
});

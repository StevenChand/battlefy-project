const LeagueJS = require('leaguejs');
const Hapi = require('hapi');

const settings = require('./settings');
const SummonerController = require('./controllers/v1/lol/summoner/controller');
const MatchController = require('./controllers/v1/lol/matches/controller');

// Initialize HapiJS webserver
const webServer = new Hapi.Server({
    connections: {
        routes: {
            cors: true,
        },
    },
});

// Initialize LeagueJS Client
const lolClient = new LeagueJS(settings.API_KEY);

webServer.connection({ port: settings.PORT, host: settings.HOST });

lolClient.updateRateLimiter({
    allowBursts: true,
});

webServer.route(SummonerController(lolClient));
webServer.route(MatchController(lolClient));

// Start the webServer
webServer.start((err) => {
    if (err) {
        throw err;
    }
    console.log(`Server running at: ${settings.HOST}:${settings.PORT}`);
});

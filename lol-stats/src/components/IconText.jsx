import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class IconText extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      text: props.text,
      iconClassName: props.iconClassName,
      className: props.className,
    };
  }

  render() {
    return (
      <div className={`bg-success p-3 text-white text-center ${this.state.className}`}>
        <i className={`ra ra-2x text-center ${this.state.iconClassName}`} />
        <span>{this.state.text}</span>
      </div>
    );
  }
}

IconText.propTypes = {
  text: PropTypes.string.isRequired,
  iconClassName: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
};

import React, { PureComponent } from 'react';
import { Radar as RadarChart } from 'react-chartjs-2';
import PropTypes from 'prop-types';

export default class PreferredLaneChart extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      chartData: {
        labels: ['Mid', 'Top', 'Jungle', 'Bot'],
        datasets: [{
          label: 'Lane Pick',
          fill: false,
          pointHoverRadius: 5,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [props.mid, props.top, props.jung, props.bot],
          spanGaps: false,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
          ],
          borderColor: [
            'rgba(255,99,132,1)',
          ],
        }],
      },
    };
  }

  render() {
    return (
      <RadarChart data={this.state.chartData} />
    );
  }
}

PreferredLaneChart.propTypes = {
  mid: PropTypes.number.isRequired,
  top: PropTypes.number.isRequired,
  jung: PropTypes.number.isRequired,
  bot: PropTypes.number.isRequired,
};

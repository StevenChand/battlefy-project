import React, { PureComponent } from 'react';
import { Pie as PieChart } from 'react-chartjs-2';
import PropTypes from 'prop-types';

export default class WinLossChart extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      chartData: {
        labels: ['Wins', 'Losses'],
        datasets: [{
          label: 'Wins/Losses',
          fill: false,
          pointHoverRadius: 5,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [props.wins, props.losses],
          spanGaps: false,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
          ],
        }],
      },
    };
  }

  render() {
    return (
      <PieChart data={this.state.chartData} />
    );
  }
}

WinLossChart.propTypes = {
  wins: PropTypes.number.isRequired,
  losses: PropTypes.number.isRequired,
};

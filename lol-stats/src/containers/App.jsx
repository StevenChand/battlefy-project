import _ from 'lodash';
import React, { Component } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Container,
  Row,
  Col,
  Button,
  Input,
  FormGroup,
} from 'reactstrap';

import IconText from '../components/IconText';
import LolStatsAPI from '../lib/LolStatsAPI';
import WinLossChart from '../components/WinLossChart';
import PreferredLaneChart from '../components/PreferredLaneChart';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      kills: 0,
      deaths: 0,
      assists: 0,
      wins: 0,
      losses: 0,
      lane: {
        mid: 0,
        top: 0,
        jung: 0,
        bot: 0,
      },
      playerInfoLoaded: false,
    };

    this.statsAPI = new LolStatsAPI();
    this.toggle = this.toggle.bind(this);
    this.generateStatsForSummoner = this.generateStatsForSummoner.bind(this);
    this.updateInputValue = this.updateInputValue.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  generateStatsForSummoner() {
    const summonerName = this.state.searchSummonerName;

    this.setState({
      playerInfoLoaded: false,
      summonerName,
    });

    this.statsAPI.getAccount(summonerName)
      .then((account) => {
        this.account = account;
        return this.statsAPI.getDetailedMatchList(account.accountId)
          .then((matchList) => {
            const newState = {
              kills: 0,
              deaths: 0,
              assists: 0,
              wins: 0,
              losses: 0,
              lane: {
                mid: 0,
                top: 0,
                jung: 0,
                bot: 0,
              },
              playerInfoLoaded: true,
            };

            _.each(matchList, (match) => {
              // Find the summoner's participantIdentity for this match
              const participantIdentity =
                match.matchDetails.participantIdentities.find(pIdentity =>
                  _.get(pIdentity, 'player.accountId') === account.accountId ||
                    _.get(pIdentity, 'player.currentAccountId', 0) === account.accountId,
                );

              if (!participantIdentity) {
                return;
              }

              // Find the summoner participant
              const participant = match.matchDetails.participants.find(part =>
                part.participantId === participantIdentity.participantId,
              );

              newState.kills += participant.stats.kills;
              newState.deaths += participant.stats.deaths;
              newState.assists += participant.stats.assists;

              if (participant.stats.win) {
                newState.wins += 1;
              } else {
                newState.losses += 1;
              }

              switch (participant.timeline.lane) {
                case 'MIDDLE':
                case 'MID':
                  newState.lane.mid += 1;
                  break;
                case 'TOP':
                  newState.lane.top += 1;
                  break;
                case 'JUNGLE':
                  newState.lane.jung += 1;
                  break;
                case 'BOTTOM':
                case 'BOT':
                  newState.lane.bot += 1;
                  break;
                default:
                  break;
              }
            });
            this.setState(newState);
          });
      })
      .catch(() => alert('Player not found'));
  }

  updateInputValue(evt) {
    this.setState({
      searchSummonerName: evt.target.value,
    });
  }

  render() {
    return (
      <div>
        <Navbar color="dark" inverse toggleable>
          <NavbarToggler onClick={this.toggle} />
          <NavbarBrand href="/">League of Legends Statifier!</NavbarBrand>
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <FormGroup className="form-inline mb-0">
                <span className="text-white mr-3">Enter a Summoner Name (NA)</span>
                <Input
                  className="form-control-static"
                  type="text"
                  onChange={this.updateInputValue}
                />
                <Button onClick={this.generateStatsForSummoner}>Statify!</Button>
              </FormGroup>
            </Nav>
          </Collapse>
        </Navbar>
        {this.state.playerInfoLoaded &&
          <Container fluid>
            <Row noGutters>
              <Col>
                <h1 className="text-center">{this.state.summonerName} (Last 20 games)</h1>
              </Col>
            </Row>
            <Row noGutters>
              <Col xs="12" sm="4">
                <IconText iconClassName="ra-axe" text={`${this.state.kills} Kills`} className="bg-success" />
              </Col>
              <Col xs="12" sm="4">
                <IconText iconClassName="ra-broken-skull" text={`${this.state.deaths} Deaths`} className="bg-danger" />
              </Col>
              <Col xs="12" sm="4">
                <IconText iconClassName="ra-double-team" text={`${this.state.assists} Assists`} className="bg-warning" />
              </Col>
            </Row>
            <Row noGutters>
              <Col xs="12" sm="6">
                <h4 className="text-center">Win/Loss</h4>
                <WinLossChart wins={this.state.wins} losses={this.state.losses} />
              </Col>
              <Col xs="12" sm="6">
                <PreferredLaneChart
                  mid={this.state.lane.mid}
                  top={this.state.lane.top}
                  jung={this.state.lane.jung}
                  bot={this.state.lane.bot}
                />
              </Col>
            </Row>
          </Container>
        }
      </div>
    );
  }
}

export default App;

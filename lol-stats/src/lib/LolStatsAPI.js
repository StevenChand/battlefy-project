import settings from '../settings';

export default class LolStatsAPI {
  constructor() {
    this.apiUrl = settings.LOL_STATS_API_URL;
  }

  getAccount(accountName) {
    return fetch(`${this.apiUrl}/v1/lol/summoner/getByName/${accountName}`)
      .then(response => Promise.resolve(response.json()));
  }

  getDetailedMatchList(accountId) {
    return fetch(`${this.apiUrl}/v1/lol/matches/detailed/getByAccount/${accountId}`)
      .then(response => Promise.resolve(response.json()));
  }
}
